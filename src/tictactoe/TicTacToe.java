/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author G A M A
 */
public class TicTacToe {
    String[][] data = new String[3][3];
    Queue replay = new Queue();
    Scanner scan = new Scanner(System.in);
    int turn=1,turnNo,mode,qx,qy;
    //constructor
    public TicTacToe() {
        //initializing with temp data
        int k=1;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                data[i][j]=""+k+"";
                k++;
            }
        }
        System.out.println(""
                + "1. Player vs Player\n"
                + "2. Player vs Computer\n"
                + "3. Computer vs Computer\n"
                + "How you want to play ?"
                + "");
        mode = scan.nextInt();
        while(mode!=1&& mode!=2 && mode!=3){
            System.out.println("Please enter a valid input (1-3)");
            mode = scan.nextInt();
        }
        //create board
        create_board();
        
        //starting game
        start_game();
        
        System.out.println("Do you want a replay ? (1. Yes || 2. No)");
        mode = scan.nextInt();
        while(mode!=1&& mode!=2){
            System.out.println("Please enter a valid input (1-3)");
            mode = scan.nextInt();
        }
        
        
       //start replay
       replay();
    }
    
    
    //function to create the board
    public void create_board(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print("|"+data[i][j]);
            }
            System.out.println("|");
        }
    }
    
    //start game
    public void start_game(){
        turnNo=1;
        while(turnNo<=9){
            if (turn==1){
                if(mode==1||mode==2){
                    player_turn(1,'X');
                    if(win_checker()==true){
                        System.out.println("Player 1 Won!!!");
                        break;
                    }
                    switch_turn();
                }
                else{
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    computer_turn(1,'X');
                    if(win_checker()==true){
                        System.out.println("Player 1 Won!!!");
                        break;
                    }
                    switch_turn();
                }
            }
            else{
                if(mode==1){
                    player_turn(2,'O');
                    if(win_checker()==true){
                        System.out.println("Player 2 Won!!!");
                        break;
                    }
                    switch_turn();
                }
                else{
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    computer_turn(2,'O');
                    if(win_checker()==true){
                        System.out.println("Player 2 Won!!!");
                        break;
                    }
                    switch_turn();
                }
            }
            create_board();
            turnNo++;
        }
    }
    
    //player turn function
    
    public void player_turn(int n, char indicator){
        int num;
        System.out.println("Player " + n +" turn : ");
        num = scan.nextInt();
        get_quadrant(num);
        while(data[qx][qy].equals("X") || data[qx][qy].equals("O")){
            System.out.println("Invalid input ... try again : ");
            num = scan.nextInt();
            get_quadrant(num);
        }
        play_move(qx,qy,n,0);
    }
    
    //Computer turn
    public void computer_turn(int player_number, char indicator){
        int x=-1,y=-1,num;
        boolean value_set=false;
        System.out.println("Player " + player_number + " turn : ");
        //horizontal checker
        for(int i=0;i<3;i++){
            if(data[i][0].equals(data[i][2])  && check_previous_mark(i, 1)){
                x=i;y=1;
                value_set=true;
            }
            else if(data[i][0].equals(data[i][1])  && check_previous_mark(i, 2)){
                x=i;y=2;
                value_set=true;
            }
            else if(data[i][1].equals(data[i][2]) && check_previous_mark(i, 0)){
                x=i;y=0;
                value_set=true;
            }
        }
        
        //vertical checker
        for(int i=0;i<3;i++){
            if(data[0][i].equals(data[2][i])  && check_previous_mark(1, i)){
                x=1;y=i;
                value_set=true;
            }
            else if(data[0][1].equals(data[1][i])  && check_previous_mark(2, i)){
                x=2;y=i;
                value_set=true;
            }
            else if(data[1][i].equals(data[2][i])  && check_previous_mark(0, i)){
                x=0;y=i;
                value_set=true;
            }
        }
        
        //diagonal checker
        if(data[0][0].equals(data[2][2])  && check_previous_mark(1, 1)){
            x=1;y=1;
            value_set=true;
        }
        else if(data[0][0].equals(data[1][1])  && check_previous_mark(2, 2)){
            x=2;y=2;
            value_set=true;
        }
        else if(data[1][1].equals(data[2][2])  && check_previous_mark(0, 0)){
            x=0;y=0;
            value_set=true;
        }
        else if(data[2][0].equals(data[0][2])  && check_previous_mark(1, 1)){
            x=1;y=1;
            value_set=true;
        }
        else if(data[2][0].equals(data[1][1])  && check_previous_mark(0, 2)){
            x=0;y=2;
            value_set=true;
        }
        else if(data[0][2].equals(data[1][1])  && check_previous_mark(2, 0)){
            x=2;y=0;
            value_set=true;
        }
        if(value_set==true){
            play_move(x, y, player_number,0);
        }
        else{
            num = ThreadLocalRandom.current().nextInt()%11  + 1;
            get_quadrant(num);
            while(data[qx][qy].equals("X") || data[qx][qy].equals("O")){
                num = ThreadLocalRandom.current().nextInt()%11  + 1;
            get_quadrant(num);
            }
            
            play_move(qx,qy,player_number,0);
        }
        
    }
    
    //switching turn
    public void switch_turn(){
        if(turn==1)
            turn=2;
        else
            turn=1;
    }
    
    //play move
    public void play_move(int x, int y, int n, int marker){
        if (n==1){
            data[x][y]=""+'O';
            if (marker==0)
                replay.Enqueue(x, y);
        }
        else if (n==2){
            data[x][y]=""+'X';
            if (marker==0)
                replay.Enqueue(x, y);
        }
    }
    
    //check win
    public boolean win_checker(){
        //vertical checker
        for(int i=0;i<3;i++){
            if(data[i][0].equals(data[i][1]) && data[i][0].equals(data[i][2])){
                create_board();
                return true;
            }
        }
        
        //horizontal checker
        for(int i=0;i<3;i++){
            if(data[0][i].equals(data[1][i]) && data[0][i].equals(data[2][i])){
                create_board();
                return true;
            }
        }
        
        //diagonal checker
        if(data[0][0].equals(data[1][1]) && data[0][0].equals(data[2][2])){
            create_board();
            return true;
        }
            
        if(data[2][0].equals(data[1][1]) && data[2][0].equals(data[0][2])){
            create_board();
            return true;
        }
            
        return false;
    }
    
    //getting quadrant against number
    
    public void get_quadrant(int num){
        if(num==1){
            qx=0;qy=0;
        }
        if(num==2){
            qx=0;qy=1;
        }
        if(num==3){
            qx=0;qy=2;
        }
        if(num==4){
            qx=1;qy=0;
        }
        if(num==5){
            qx=1;qy=1;
        }
        if(num==6){
            qx=1;qy=2;
        }
        if(num==7){
            qx=2;qy=0;
        }
        if(num==8){
            qx=2;qy=1;
        }
        if(num==9){
            qx=2;qy=2;
        }
    }
    
    public boolean check_previous_mark(int x,int y){
        if (data[x][y].equals("X")||data[x][y].equals("O"))
            return false;
        return true;
    }
    
    
    public void replay (){
        int k=1;
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                data[i][j]=""+k+"";
                k++;
            }
        }
        for (int i =0;i<replay.rear;i++){
            if (i%2==0){
                System.out.println("Player 1 : ");
                replay.Dequeue();
                play_move(replay.x_component, replay.y_component, 1, 1);
                create_board();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else{
                System.out.println("Player 2 : ");
                replay.Dequeue();
                play_move(replay.x_component, replay.y_component, 2, 1);
                create_board();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
