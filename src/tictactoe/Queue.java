/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

/**
 *
 * @author G A M A
 */
public class Queue {
    static Node[] n = new Node[9];
    static int front,rear,x_component,y_component;
    public Queue() {
        front=0;rear=0;
        for(int i = 0;i<9;i++){
            n[i] = new Node();
        }
    }
    
    static void Enqueue(int x,int y) { 
        // check queue is full or not 
        if ( 9 == rear) { 
            System.out.printf("\nQueue is full\n");
        } 
  
        // insert element at the rear 
        else { 
            n[rear].x = x;
            n[rear].y = y;
            rear++; 
        } 
    } 
    
    
    static void Dequeue() 
    { 
        // if queue is empty 
        if (front == rear) { 
            System.out.printf("\nQueue is empty\n"); 
        } 
  
        // shift all the elements from index 2 till rear 
        // to the right by one 
        else { 
            // store 0 at rear indicating there's no element 
            x_component = n[front].x;
            y_component = n[front].y;
            front++;
        } 
    } 
    
    
}
